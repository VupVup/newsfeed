import requests
from bs4 import BeautifulSoup
from sqlalchemy.ext.declarative import declarative_base
from bottle import route, run, template
from bottle import redirect, request
from sqlalchemy import Column, String, Integer
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
# from nltk.stem.snowball import SnowballStemmer
# from nltk.corpus import stopwords
import math
Base = declarative_base()


class News(Base):
    __tablename__ = "news"
    id = Column(Integer, primary_key=True)
    title = Column(String)
    author = Column(String)
    tags = Column(String)
    comments = Column(Integer)
    label = Column(String)


class Rate(Base):
    __tablename__ = 'rating'
    id = Column(Integer, primary_key=True)
    lable = Column(String)
    author = Column(String)
    num_of_author = Column(Integer)
    tags = Column(String)
    num_of_tags = Column(Integer)
    num_of_comm = Column(Integer)
    word = Column(String)
    num_of_word = Column(Integer)


engine = create_engine("sqlite:///news.db")
Base.metadata.create_all(bind=engine)
engine1 = create_engine("sqlite:///Ratings.db")
Base.metadata.create_all(bind=engine1)

session = sessionmaker(bind=engine)
session1 = sessionmaker(bind=engine1)
s = session()
s1 = session1()


def get_news(link):
    return_list = []
    r = requests.get(link)
    page = BeautifulSoup(r.text, 'html.parser')
    articles = page.body.findAll('span', attrs={"itemprop": "name"})
    articles_list = [article.text for article in articles]
    autors = page.body.findAll('span', attrs={"class": "autor"})
    autors_list = [autor.text for autor in autors]
    tags = page.body.findAll('div', attrs={"class": "meta"})
    tags_list = [tag.text for tag in tags]
    comment_num = page.body.findAll(attrs={"class": "v-count"})
    comment_num_list = [comment.text for comment in comment_num]
    for art, aut, tag, com in zip(articles_list, autors_list, tags_list, comment_num_list):
        dict = {'author': aut,
                'articles': art,
                'tags': tag,
                'number of comments': com}
        return_list.append(dict)
    return return_list


@route('/add_label/')
def add_label():
    label = request.GET.get('label')
    idi = request.GET.get('id')
    our_note = s.query(News).filter_by(id=idi).first()
    our_note.label = label
    s.commit()
    redirect('/news')

# stemmer = SnowballStemmer("russian")
# stop = set(stopwords.words('russian'))


def words_to_number(labelz):
    query = s.query(News).filter_by(label=labelz).all()
    dict_auth = {}
    dict_tags = {}
    unic_words = {}
    for i in query:
        # sentense = i.title.split()
        words = [word for word in i.title.lower().split() if word not in stop]
        for word in words:
            try:
                unic_words[stemmer.stem(word)] += 1
            except:
                unic_words[stemmer.stem(word)] = 1
        try:
            dict_auth[i.author] += 1
        except:
            dict_auth[i.author] = 1
        for j in i.tags.split(','):
            if len(j) == 0:
                print('sos')
                j = 'a'
            try:
                while ord(j[0]) <= 35:
                    j = j[1:len(j)]
            except:
                 pass
            try:
                dict_tags[j] += 1
            except:
                dict_tags[j] = 1
    for tag in dict_tags:
        tages = Rate(tags=tag,
                     num_of_tags=dict_tags[tag],
                     lable=labelz)
        s1.add(tages)
        s1.commit()
    for auth in dict_auth:
        authors = Rate(author=auth,
                     num_of_author=dict_auth[auth],
                       lable=labelz)
        s1.add(authors)
        s1.commit()
    for word_ in unic_words:
        words = Rate(lable=labelz,
                     word=word_,
                     num_of_word=unic_words[word_])
        s1.add(words)
        s1.commit()


# for i in range(1, 35):
#     news_list = get_news('http://4pda.ru/page/{}/'.format(i))
#     for niw in news_list:
#         news = News(title=niw['articles'],
#                     author=niw['author'],
#                     tags=niw['tags'],
#                     comments=niw['number of comments'])
#         s.add(news)
#         s.commit()

@route('/')
@route('/news')
def news_list():
    s = session()
    rows = s.query(News).filter(News.label == None).all()
    return template('news_template', rows=rows)

@route('/sorted_news')
def sorted_list():
    s = session()
    rows1 = s.query(News).filter(News.label == '1').all()
    rows2 = s.query(News).filter(News.label == '2').all()
    rows3 = s.query(News).filter(News.label == '3').all()
    return template('intresting', rows1=rows1, rows2=rows2, rows3=rows3)

@route('/back')
def back():
    redirect('/news')

@route('/update_news')
def update_news():
    news_list = get_news('http://4pda.ru/')
    flag = True
    for niw in news_list:
        query = s.query(News).filter(News.author == niw['author']).all() #filter().count()
        for title in query:
            if title.title == niw['articles']:
                print('1')
                flag = False
                break
        if flag:
            news = News(title=niw['articles'],
                        author=niw['author'],
                        tags=niw['tags'],
                        comments=niw['number of comments'])
            s.add(news)
            s.commit()
    redirect('/news')

run(host='localhost', port=8080)

# stemmer = SnowballStemmer("russian")
# stop = set(stopwords.words('russian'))

labels = ['good', 'maybe', 'never']

# for i in labels:
#     words_to_number(i)

def possibility(label):
    total = len(s.query(News).filter(News.label != None).all())
    label_poss = len(s.query(News).filter(News.label == label).all())
    return label_poss/total

def possibility_author(label, author):
    total = len(s.query(News).filter(News.label == label).filter(News.author != None).all())
    author_poss = s1.query(Rate).filter(Rate.lable == label).filter(Rate.author == author).first()
    try:
        return author_poss.num_of_author / total
    except AttributeError:
        return 1

def possibility_tag(label, tag):
    total = 0
    query = s1.query(Rate).filter(Rate.lable == label).filter(News.tags != None).all()
    for tags in query:
        total += tags.num_of_tags
    tags_poss = s1.query(Rate).filter(Rate.lable == label).filter(Rate.tags == tag).first()
    try:
        return tags_poss.num_of_tags / total
    except AttributeError:
        return 1

def possibility_word(label, word_):
    total = 0
    query = s1.query(Rate).filter(Rate.lable == label).filter(Rate.word != None).all()
    for tags in query:
        total += tags.num_of_word + 1
    word_poss = s1.query(Rate).filter(Rate.lable == label).filter(Rate.word == word_).first()
    try:
        return word_poss.num_of_word / total
    except AttributeError:
        return 1


# @route('/sorting')
def classificator():
    unlabled_news = s.query(News).filter(News.label == None).all()
    for news in unlabled_news:
        label_dict = {'good' : 0,
                      'maybe' : 0,
                      'never' : 0}
        for label in labels:
            poss_auth = math.log(possibility_author(label, news.author))
            poss_word = 0
            words = [word for word in news.title.lower().split()]  # if word not in stop]
            for word in words:
                poss_word += math.log(possibility_word(label, word))  #stemmer.stem(word)))
            poss_tag = 0
            for tag in news.tags.split(','):
                poss_tag += math.log(possibility_tag(label, tag))
            label_dict[label] = poss_auth + poss_tag + poss_word + possibility(label)
        max = float('-inf')
        key = ''
        for label in label_dict:
            if label_dict[label] > max:
                max = label_dict[label]
                key = label
        if key == 'good':
            news.label = '1'
        elif key == 'maybe':
            news.label = '2'
        else:
            news.label = '3'
        s.commit()
    redirect('/news')
# classificator()

