﻿<!-- news_template.tpl -->
<table border=1>
    <tr>
	<th>id</th>
        <th>title</th>
        <th>author</th>
        <th>#comments</th>
        <th>#tags</th>
        <th colspan="3">Label</th>
    </tr>
    %for row in rows:
        <tr >
	    <td>{{row.id}}</td>
            <td>{{row.title}}</td>
            <td >{{row.author}}</td>
            <td>{{row.comments}}</td>
            <td>{{row.tags}}</td>
            <td><a href="/add_label/?label=good&id={{row.id}}">Интересно</a></td>
            <td><a href="/add_label/?label=maybe&id={{row.id}}">Возможно</a></td>
            <td><a href="/add_label/?label=never&id={{row.id}}">Не интересно</a></td>
        </tr>
    %end
</table>
<a href="/update_news">I Wanna more 4PDA NEWS!</a>
<a href="/sorting">Сортировать новости</a>
<a href="/sorted_news">Сортированные новости</a>